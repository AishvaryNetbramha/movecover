// hero section carousal
$(document).ready(function() {
    var owl = $("#hero-section"); 

    $('#hero-section').owlCarousel({
        loop: true,
        nav: false,
        dots: true,
        autoplay: false,
        center: true,
        autoplayHoverPause: true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 1
          },
          1000: {
            items: 1
          }
        }
      })

});
